import json
import pandas as pd
from pandas.io.json import json_normalize
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
from sklearn.model_selection import train_test_split
import numpy as np
import heapq
from collections import Counter
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure


data = []

# change the range to decide what files to load
# if non sequential files need to be loaded simply loop through list with The
# numbers of the files to be loaded

for i in range(16, 26):
    with open(f'tu_{i}.json') as f:
        data = data +  [json.loads(line) for line in f]

# for loading a single file:

# with open('tu_21.json') as f:
#     data = [json.loads(line) for line in f]

# converting json dataset from dictionary to dataframe
train = pd.DataFrame.from_dict(data, orient='columns')

df = train
target = "solution_status"

# code for counting over the path switch ids and adding them to the dataframe.
# commented because this only applies to the files where this data is present

# if 'path_switch_ids' in list(df.columns):
#     list_counter = []
#     max_numbers = []
#     for entry in df['path_switch_ids']:
#         bulldozed_list = []
#         for list in entry:
#             bulldozed_list = bulldozed_list + list
#         max_numbers.append(np.amax(bulldozed_list))
#         list_counter.append(Counter(bulldozed_list).most_common())
#
#     max_track_id = np.amax(max_numbers)+1
#     column_names = [i for i in range(1, max_track_id)]
#     turbo_lijst = []
#     for unsorted in list_counter:
#         turbo_lijst.append(sorted(unsorted))
#
#     total_column_values = []
#     for list in turbo_lijst:
#         counter = 1
#         single_column_value = []
#         for combo in list:
#             while combo[0] != counter:
#                 single_column_value.append(0)
#                 counter+=1
#             single_column_value.append(combo[1])
#             counter+=1
#         while counter < max_track_id:
#             single_column_value.append(0)
#             counter+=1
#         total_column_values.append(single_column_value)
#
#     array =  np.asarray(total_column_values)
#     new_df = pd.DataFrame(array, columns = column_names).astype(float)
#
#     for column in column_names:
#         df[column] = pd.Series(new_df[column])

def clean_up(df):
    """
    function for removing unnecessary and incomaptible data
    returns the cleaned dataframe and the columns dropped from the dataframe
    Input:
        df = pandas dataframe containing the data
    """
    exc_columns = ["_id", "probability", "solution_id", "instance_id"]
    for column in df.columns:
        try:
            # check if a column has only 1 unique value, if so drop it
            unique = df[column].nunique()
            if unique == 1:
                df = df.drop(column, axis=1)
                exc_columns.append(column)
        except TypeError:
            # if the column contains incompatible data it should be dropped
            df = df.drop(column, axis=1)
            exc_columns.append(column)

    # all unnecessary data
    df = df.drop("_id", axis=1)
    df = df.drop("probability", axis=1)
    df = df.drop("solution_id", axis=1)
    df = df.drop("instance_id", axis=1)

    # used to take a random sample of the data, can be removed is the machine
    # can store all the data in its entirety.
    df = df.sample(frac=.75)
    return df, exc_columns
df, exc_columns = clean_up(df)

# splits the data into a train and test set
train_test = train_test_split(df, train_size=.7)
set_train = train_test[0]
set_test = train_test[1]

# creates the targer columns for both sets
set_train_target = set_train[target]
set_test_target = set_test[target]
set_train_dropped = set_train.drop(target, axis=1)
set_test_dropped = set_test.drop(target, axis=1)

# creating and fitting of the single tree
# max_featues and max_depth parameters are the values found to be working
# best for this set, criterion is gini as that is the measure used by
# feature_importance_ method
tree_d  = DecisionTreeClassifier(max_features = "sqrt", max_depth = 15, criterion = "gini")
tree_d.fit(set_train_dropped, set_train_target)

train_prediction = tree_d.predict(set_train_dropped)
test_prediction = tree_d.predict(set_test_dropped)

# the different evaluation methods
print(f'training accuracy is {metrics.accuracy_score(set_train_target, train_prediction)*100}%')
print(f'test accuracy is {metrics.accuracy_score(set_test_target, test_prediction)*100}%')

print(f'Accuracy report training set:\n{metrics.classification_report(set_train_target, train_prediction)}')
print(f'Accuracy report test set:\n{metrics.classification_report(set_test_target, test_prediction)}')

print(f'Balanced accuracy score training set {metrics.balanced_accuracy_score(set_train_target, train_prediction)}')
print(f'Balanced accuracy score test set {metrics.balanced_accuracy_score(set_test_target, test_prediction)}')

print(f'Adjusted balanced accuracy score training set {metrics.balanced_accuracy_score(set_train_target, train_prediction, adjusted=True)}')
print(f'Adjusted balanced accuracy score test set {metrics.balanced_accuracy_score(set_test_target, test_prediction, adjusted=True)}')


def create_forest(n_trees, max_depth = 15):
    """
    function to initialize the trees of the forest (does not fit the trees)
    return list with untrained desiscion trees
    additional parameters have to be edited in function itself
    Input:
        max_depth = positive int, default = 15
        n_trees = positive int
    """
 return [DecisionTreeClassifier(max_features = max_features, max_depth = max_depth, criterion = criterion) for n in range(n_trees)]

def train_forest(forest, data, target, ratio=0.5):
    """
    function to fit all the trees in the forest on a dataset
    also used to keep track of and plot the least and most important features of
    every tree, for explainability. Does this by counting how often each feature
    occurs in the top and bottom 5 of every tree.
    Returns a trained forest
    Input:
        forest = list of unfitted DecisionTreeClassifier objects
        data = pandas dataframe, the training set
        target = string, name of the target column fo the dataset
        ratio = positive float, fraction of the training set to be used for fitting every tree
    """
    new_forest = []
    index_list_max = []
    index_list_min = []

    for tree_d in forest:
        # take sample of dataset and seperate target and actual data
        train_forest = data.sample(frac=ratio)
        train_forest_target = train_forest[target]
        train_forest = train_forest.drop(target, axis=1)

        # fit the tree and put it in the list
        fitted = tree_d.fit(train_forest, train_forest_target)
        new_forest.append(fitted)

        # define axes for importance plots
        X = train_forest.columns.tolist()
        Y = fitted.feature_importances_

        # take the 5 largest and smallest importances
        largest = heapq.nlargest(5, Y)
        smallest = heapq.nsmallest(5, Y)

        # take the indeces of the most and least important features
        max_indexes = [Y.tolist().index(i) for i in largest]
        min_indexes = [Y.tolist().index(i) for i in smallest]

        # add the new entries to the lists
        index_list_max = index_list_max+max_indexes
        index_list_min = index_list_min+min_indexes

        # take the name of the corresponding columns for the indeces
        max_columns = [train_forest.columns[j] for j in max_indexes]

    # count how often each index occurs in the list, should only be as long as
    # the number of unique indeces
    counter_list_max = Counter(index_list_max).most_common(len(set(index_list_max)))
    counter_list_max = sorted(counter_list_max, key = lambda x:x[1], reverse=True)
    feat_max = []
    feat_freq_max = []
    for i in counter_list_max:
        feat_max.append(X[i[0]])
        feat_freq_max.append(i[1])
    plt.barh(range(len(feat_max)), feat_freq_max, color = "#00387b")
    plt.xlabel("Figure 1: Amount of occurences in top 5 most important features")
    plt.yticks(range(len(feat_max)), feat_max, fontsize=6)
    plt.savefig('most_important', bbox_inches='tight', transparent = True, dpi=800, path_inches = 0.0)
    plt.show()

    # same code as for the best features but for the worst ones
    counter_list_min = Counter(index_list_min).most_common(len(set(index_list_min)))
    counter_list_min = sorted(counter_list_min, key = lambda x:x[1], reverse=True)
    feat_min = []
    feat_freq_min = []
    for i in counter_list_min:
        feat_min.append(X[i[0]])
        feat_freq_min.append(i[1])
    plt.barh(range(len(feat_min)), feat_freq_min, color = "#00387b")
    plt.xlabel("Figure 2: Amount of occurences in top 5 least important features")
    plt.yticks(range(len(feat_min)), feat_min, fontsize=6)
    plt.savefig('least_important', bbox_inches='tight', transparent = True, dpi=800, path_inches = 0.0)
    plt.show()

    return new_forest

def predict_forest(forest, data):
    """
    Makes the predictions of the forest on the given data.
    Works only for binary classification tasks.
    returns a prediction for every row in the data, also returns the unrounded
    means of the prediction for a certainty measure. Both returned items are lists
    Input:
        forest = list containing fitted DecisionTreeClassifier objects
        data = pandas dataframe, the data you want to predict
    """
    # every tree in the forest makes its prediction
    total_sum = [tree_d.predict(data.drop(target, axis=1)) for tree_d in forest]

    # take the means of the predictions for every data point
    pre_pred = np.mean(total_sum, axis = 0)

    # round every data to the nearest int
    predictions = np.around(pre_pred)
    return predictions, pre_pred

forest = create_forest(1000)
trained_forest = train_forest(forest, set_train, target)
train_predictions, train_certainty = predict_forest(trained_forest, set_train)
test_predictions, test_certainty = predict_forest(trained_forest, set_test)

# determines the average certainty of predictions
train_certainty = [certainty if certainty>.5 else 1-certainty for certainty in train_certainty]
test_certainty = [certainty if certainty>.5 else 1-certainty for certainty in test_certainty]

# the different evaluation methods
print(f'The average training certainty of the model is {np.mean(train_certainty)*100}%')
print(f'The average test certainty of the model is {np.mean(test_certainty)*100}%')

print(f'The accuracy on the training set is {metrics.accuracy_score(set_train_target, train_predictions)*100}%')
print(f'The accuracy on the test set is {metrics.accuracy_score(set_test_target, test_predictions)*100}%')

print(f'Accuracy report training set:\n{metrics.classification_report(set_train_target, train_predictions)}')
print(f'Accuracy report test set:\n{metrics.classification_report(set_test_target, test_predictions)}')

print(f'Balanced accuracy score training set {metrics.balanced_accuracy_score(set_train_target, train_predictions)}')
print(f'Balanced accuracy score test set {metrics.balanced_accuracy_score(set_test_target, test_predictions)}')

print(f'Adjusted balanced accuracy score training set {metrics.balanced_accuracy_score(set_train_target, train_predictions, adjusted=True)}')
print(f'Adjusted balanced accuracy score test set {metrics.balanced_accuracy_score(set_test_target, test_predictions, adjusted=True)}')